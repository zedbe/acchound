from flask_wtf import FlaskForm
from wtforms.fields import StringField, PasswordField, BooleanField, SubmitField, TextAreaField, SelectField
from wtforms.fields.html5 import DateField, IntegerField
from wtforms.validators import DataRequired, Optional


class LoginForm(FlaskForm):
    jmeno = StringField('jméno:', validators=[DataRequired()])
    heslo = PasswordField('heslo:', validators=[DataRequired()])
    remember_me = BooleanField('Pamatuj si mě')
    submit = SubmitField('Přihlásit')


class HoundForm(FlaskForm):
    hlidac = IntegerField('Počet dnů:', validators=[DataRequired()])
    submit = SubmitField('Hlídej!')


class CreateRole(FlaskForm):
    nazev = StringField('název:', validators=[DataRequired()])
    popisek = TextAreaField('popisek:', validators=[DataRequired()])
    submit = SubmitField('Vytvořit')


class CreateClient(FlaskForm):
    jmeno = StringField('jméno:', validators=[DataRequired()])
    adresa = TextAreaField('adresa:', validators=[DataRequired()])
    telefon = StringField('telefon:', validators=[DataRequired()])
    email = StringField('email:')
    submit = SubmitField('Vytvořit')


class EditClient(FlaskForm):
    jmeno = StringField('jméno:', validators=[DataRequired()])
    adresa = TextAreaField('adresa:', validators=[DataRequired()])
    telefon = StringField('telefon:', validators=[DataRequired()])
    spravce_id = SelectField('spravce:', choices=[], coerce=int, validators=[DataRequired()])
    email = StringField('email:')
    submit = SubmitField('Upravit')


class CreateRecord(FlaskForm):
    typ = SelectField('typ:', choices=[], coerce=int, validators=[DataRequired()])  
    dodani = DateField('dodáno:', format='%Y-%m-%d', validators=(Optional(),))
    uzaverka = DateField('uzávěrka:', format='%Y-%m-%d', validators=[DataRequired()])
    zpracovani = DateField('zpracováno:', format='%Y-%m-%d', validators=(Optional(),))
    submit = SubmitField('Vytvořit')


class EditRecord(FlaskForm):
    dodani = DateField('dodáno:', format='%Y-%m-%d', validators=(Optional(),))
    uzaverka = DateField('uzávěrka:', format='%Y-%m-%d', validators=[DataRequired()])
    zpracovani = DateField('zpracováno:', format='%Y-%m-%d', validators=(Optional(),))
    submit = SubmitField('Upravit')
