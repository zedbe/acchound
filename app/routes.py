from flask import request, flash, url_for, redirect, render_template, Blueprint
from flask_login import login_user, current_user, logout_user
from werkzeug.urls import url_parse
from werkzeug.datastructures import MultiDict
from datetime import datetime, timedelta
from flask_mail import Message

import models as m
import forms as f

blueprint = Blueprint('routes', __name__)


@blueprint.route('/', methods=['GET', 'POST'])
@blueprint.route('/index', methods=['GET', 'POST'])
def index():
    if current_user.is_authenticated:
        spravce = m.db.session.query(m.Zamestnanci).filter(m.Zamestnanci.id_zamestnance == current_user.id_zamestnance).first()
        treshold = datetime.now().date() + timedelta(days=spravce.hlidac)
        
        if request.method == 'GET':
            form = f.HoundForm(formdata=MultiDict({'hlidac': spravce.hlidac}))
        else:
            form = f.HoundForm()

        if form.validate_on_submit():
            spravce.hlidac = form.hlidac.data if form.hlidac else spravce.hlidac
            
            m.db.session.commit()
            flash('Hlídač byl úspěšně nastaven', 'success')
            return redirect(url_for('routes.index'))

        # vsechny nezpracovane dokumenty, nebo nedodane
        others = m.db.session.query(m.Zamestnanci, m.Klienti, m.DocsKlientu, m.TypyDokumentu) \
            .join(m.Klienti) \
            .join(m.DocsKlientu) \
            .join(m.TypyDokumentu) \
            .filter((m.DocsKlientu.dodani == None) | (m.DocsKlientu.zpracovani == None))\
            .filter(m.Klienti.spravce_id == current_user.id_zamestnance)\
            .order_by(m.DocsKlientu.uzaverka)

        crit = others.filter(m.DocsKlientu.uzaverka <= treshold).all()
        others = others.filter(treshold < m.DocsKlientu.uzaverka).all()

        return render_template('index.html', crit_form=crit, others_form=others, form=form, spravce=spravce)

    return render_template('index.html')


@blueprint.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('routes.index'))

    form = f.LoginForm()

    if form.validate_on_submit():
        user = m.Zamestnanci.query.filter_by(jmeno=form.jmeno.data).first()
        if user is None or not user.check_password(form.heslo.data):
            flash('Invalid username or password', 'error')
            return redirect(url_for('routes.login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('routes.index')
        return redirect(next_page)

    return render_template('login.html', title='Sign In', form=form)


@blueprint.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('routes.index'))


@blueprint.route('/roles')
def roles():
    if not current_user.is_authenticated:
        return redirect(url_for('routes.login'))
    return render_template('roles.html', title='Role', Role=m.Role.query.all())


@blueprint.route('/roles/new', methods=['GET', 'POST'])
def newRole():
    if not current_user.is_authenticated:
        return redirect(url_for('routes.login'))
    form = f.CreateRole()
    if form.validate_on_submit():
        r = m.Role(nazev=form.nazev.data, popisek=form.popisek.data)
        m.db.session.add(r)
        m.db.session.commit()
        flash('Role byla úspěšně přidána', 'success')
        return redirect(url_for('routes.roles'))
    return render_template('new.html', title='Nová role', form=form)


@blueprint.route('/clients')
def clients():
    klienti = m.Klienti.query.filter(m.Klienti.spravce_id == current_user.id_zamestnance)
    return render_template('klienti.html', title="Klienti of " + str(current_user.id_zamestnance), Klienti=klienti)


@blueprint.route('/clients/new', methods=['GET', 'POST'])
def newClient():
    if not current_user.is_authenticated:
        return redirect(url_for('routes.login'))
    form = f.CreateClient()
    if form.validate_on_submit():
        k = m.Klienti(jmeno=form.jmeno.data, email=form.email.data, adresa=form.adresa.data, telefon=form.telefon.data,
                      role_id=2, spravce_id=current_user.id_zamestnance)
        m.db.session.add(k)
        m.db.session.commit()
        flash('Klient byl úspěšně přidán', 'success')
        return redirect(url_for('routes.clients'))
    return render_template('new.html', klient='Nový klient', form=form)


@blueprint.route('/client/<int:id>/', methods=['GET', 'POST'])
def editClient(id):
    if not current_user.is_authenticated:
        return redirect(url_for('routes.login'))
    klient = m.db.session.query(m.Klienti).get(id)
    spravci = [(spravce.id_zamestnance, spravce.jmeno) for spravce in m.Zamestnanci.query.all()]

    if request.method == 'GET':
        form = f.EditClient(formdata=MultiDict({'jmeno': klient.jmeno, 'email': klient.email,
                                                'adresa': klient.adresa, 'telefon': klient.telefon,
                                                'spravce_id': klient.spravce_id}))
    else:
        form = f.EditClient()

    form.spravce_id.choices = spravci

    if form.validate_on_submit():
        klient.jmeno = form.jmeno.data if form.jmeno else klient.jmeno
        klient.email = form.email.data if form.email else klient.email
        klient.adresa = form.adresa.data if form.adresa else klient.adresa
        klient.telefon = form.telefon.data if form.telefon else klient.telefon

        spravce = m.Zamestnanci.query.filter(m.Zamestnanci.id_zamestnance == form.spravce_id.data).first()
        klient.spravce_id = spravce.id_zamestnance

        m.db.session.commit()
        flash('Klient byl úspěšně změněn', 'success')
        return redirect(url_for('routes.clients'))
    return render_template('klient.html', title='Úprava klienta', klient=klient.jmeno, form=form)


@blueprint.route('/deleteClient/<string:id>', methods=['GET'])
def deleteClient(id):
    if not current_user.is_authenticated:
        return redirect(url_for('routes.login'))

    klient = m.db.session.query(m.Klienti).get(id)
    m.db.session.delete(klient)
    m.db.session.commit()

    flash('Klient smazán', 'success')

    return redirect(url_for('routes.clients'))


@blueprint.route('/client/<string:id>/evidence')
def docs(id):
    if not current_user.is_authenticated:
        return redirect(url_for('routes.login'))

    docs = m.db.session.query(m.DocsKlientu, m.TypyDokumentu)\
        .join(m.TypyDokumentu)\
        .filter(m.DocsKlientu.klient_id == id).order_by(m.DocsKlientu.id_zaznamu.desc())

    klient = m.db.session.query(m.Klienti).get(id)
    return render_template('docs.html', klient=klient.jmeno, title="Evidence", id=id, docs=docs)


@blueprint.route('/client/<int:id>/evidence/new', methods=['GET', 'POST'])
def newRecord(id):
    if not current_user.is_authenticated:
        return redirect(url_for('routes.login'))

    klient = m.db.session.query(m.Klienti).get(id)
    form = f.CreateRecord()
    typy = [(typ.id_dokumentu, typ.typ) for typ in m.TypyDokumentu.query.all()]

    form.typ.choices = typy

    if form.validate_on_submit():
        typ = m.TypyDokumentu.query.filter(m.TypyDokumentu.id_dokumentu == form.typ.data).first()

        statement = m.DocsKlientu(klient_id=id, dokument_id=typ.id_dokumentu, uzaverka=form.uzaverka.data,
                                  dodani=form.dodani.data, zpracovani=form.zpracovani.data)
        m.db.session.add(statement)
        m.db.session.commit()
        flash('Záznam byl úspěšně přidán', 'success')
        return redirect(url_for('routes.docs', id=id))
    return render_template('new.html', small="Nový záznam klienta:", klient=klient.jmeno, form=form)


@blueprint.route('/client/<int:client_id>/evidence/<int:record_id>', methods=['GET', 'POST'])
def editRecord(client_id, record_id):
    if not current_user.is_authenticated:
        return redirect(url_for('routes.login'))
    klient = m.db.session.query(m.Klienti).get(client_id)
    zaznam = m.db.session.query(m.DocsKlientu).get(record_id)
    if request.method == 'GET':
        form = f.EditRecord(formdata=MultiDict({'dodani': str(zaznam.dodani),
                                                'uzaverka': str(zaznam.uzaverka),
                                                'zpracovani': str(zaznam.zpracovani)}))
    else:
        form = f.EditRecord()

    if form.validate_on_submit():
        zaznam.dodani = form.dodani.data if form.dodani else zaznam.dodani
        zaznam.uzaverka = form.uzaverka.data if form.uzaverka else zaznam.uzaverka
        zaznam.zpracovani = form.zpracovani.data if form.zpracovani else zaznam.zpracovani

        m.db.session.commit()
        flash('Záznam byl úspěšně upraven', 'success')
        return redirect(url_for('routes.docs', id=client_id))
    return render_template('doc.html', title='Evidence záznamu klienta', klient=klient.jmeno, form=form)

