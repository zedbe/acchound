from flask_login import UserMixin, LoginManager
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash

db = SQLAlchemy()
login_manager = LoginManager()
login_manager.login_view = 'login'


class Role(db.Model):
    __tablename__ = 'Role'

    id_role = db.Column(db.Integer, primary_key=True)
    nazev = db.Column(db.String(255), nullable=False)
    popisek = db.Column(db.String(255))


class Zamestnanci(db.Model, UserMixin):
    __tablename__ = 'Zamestnanci'

    id_zamestnance = db.Column(db.Integer, primary_key=True)
    jmeno = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False)
    heslo = db.Column(db.String(255), nullable=False)
    hlidac = db.Column(db.Integer)
    role_id = db.Column(db.Integer, db.ForeignKey('Role.id_role'), nullable=False)

    def set_password(self, password):
        self.heslo = generate_password_hash(password)

    def check_password(self, password):
        # why generate in check? bc I have already unhashed pass in DB and 1st parameter has to be hashed pass
        return check_password_hash(generate_password_hash(self.heslo), password)

    def get_id(self):
        return self.id_zamestnance

    def __repr__(self):
        return '<Zamestnanec: {}>'.format(self.jmeno)


class Klienti(db.Model, UserMixin):
    __tablename__ = 'Klienti'

    id_klienta = db.Column(db.Integer, primary_key=True)
    jmeno = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False)
    adresa = db.Column(db.String(255), nullable=False)
    telefon = db.Column(db.String(255), nullable=False)
    # heslo = db.Column(db.String(255), nullable=False)
    spravce_id = db.Column(db.Integer, db.ForeignKey('Zamestnanci.id_zamestnance'), nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey('Role.id_role'), nullable=False)
    typy = db.relationship("TypyDokumentu", secondary='Docs_Klientu')

    def get_id(self):
        return self.id_klienta

    def __repr__(self):
        return '<Klient: {}>'.format(self.jmeno)


class TypyDokumentu(db.Model, UserMixin):
    __tablename__ = 'Typy_dokumentu'

    id_dokumentu = db.Column(db.Integer, primary_key=True)
    typ = db.Column(db.String(255), nullable=False)
    frekvence = db.Column(db.String(255), nullable=False)
    klienti = db.relationship("Klienti", secondary='Docs_Klientu')

    def get_id(self):
        return self.id_dokumentu

    def __repr__(self):
        return '<Dokument typu: {}>'.format(self.typ)


class DocsKlientu(db.Model, UserMixin):
    __tablename__ = 'Docs_Klientu'

    id_zaznamu = db.Column(db.Integer, primary_key=True)
    klient_id = db.Column(db.Integer, db.ForeignKey('Klienti.id_klienta'))
    dokument_id = db.Column(db.Integer, db.ForeignKey('Typy_dokumentu.id_dokumentu'))
    uzaverka = db.Column(db.Date, nullable=False)
    dodani = db.Column(db.Date, nullable=True)
    zpracovani = db.Column(db.Date, nullable=True)

    klient = db.relationship(Klienti, backref=db.backref("klient_assoc"))
    typ = db.relationship(TypyDokumentu, backref=db.backref("typ_assoc"))

    def get_id(self):
        return self.id_zaznamu


# Set up user_loader
@login_manager.user_loader
def load_user(id):
    return Zamestnanci.query.get(int(id))
