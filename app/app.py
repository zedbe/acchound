from flask import Flask
from flask_bootstrap import Bootstrap
import models
from routes import blueprint

app = Flask(__name__)

# ORM, DB setup
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root@db:3306/acchound'
app.config['SECRET_KEY'] = "random string"
app.config['SQLALCHEMY_ECHO'] = True

# preventing circular dependancy errors
app.register_blueprint(blueprint)

models.db.init_app(app)
models.login_manager.setup_app(app)
bootstrap = Bootstrap(app)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
