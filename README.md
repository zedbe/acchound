quick guide how to run:
 - `docker-compose` in directory with `docker-compose.yml` will build and runs docker containers separately.
 
 - Container with mysql DB will be initialized after start.
 - Flask runs in debug mode, that means you can update source code in `app` directory, and web service will apply those changes
 - web app accessible on `0.0.0.0:5000` or `localhost:5000`
 