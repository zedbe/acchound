-- init schema for AccHound application ----------------------------------------------
CREATE DATABASE acchound;
use acchound;

CREATE TABLE IF NOT EXISTS Role(
  id_role INT(11) NOT NULL AUTO_INCREMENT,
  nazev VARCHAR(255) NOT NULL,
  popisek VARCHAR(255),

  PRIMARY KEY (id_role)
);

CREATE TABLE IF NOT EXISTS Zamestnanci(
  id_zamestnance INT(11) NOT NULL AUTO_INCREMENT,
  jmeno VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  heslo VARCHAR(255) NOT NULL,
  hlidac INT(11) DEFAULT 7,

  role_id INT(11) NOT NULL,

  PRIMARY KEY (id_zamestnance),
  FOREIGN KEY (role_id) REFERENCES Role(id_role)
);

CREATE TABLE IF NOT EXISTS Klienti(
  id_klienta INT(11) NOT NULL AUTO_INCREMENT,
  jmeno VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  adresa VARCHAR(255) NOT NULL,
  telefon VARCHAR(255) NOT NULL,

  spravce_id INT(11) NOT NULL,
  role_id INT(11) NOT NULL,

  PRIMARY KEY (id_klienta),
  FOREIGN KEY (spravce_id) REFERENCES Zamestnanci(id_zamestnance),
  FOREIGN KEY (role_id) REFERENCES Role(id_role)
);

CREATE TABLE IF NOT EXISTS Typy_dokumentu(
  id_dokumentu INT(11) NOT NULL AUTO_INCREMENT,
  typ VARCHAR(255) NOT NULL,
  frekvence VARCHAR(255),

  PRIMARY KEY (id_dokumentu)
);

CREATE TABLE IF NOT EXISTS Docs_Klientu(
  id_zaznamu INT(11) NOT NULL AUTO_INCREMENT,
  klient_id INT(11) NOT NULL,
  dokument_id INT(11) NOT NULL,
  uzaverka DATE NOT NULL,
  dodani DATE,
  zpracovani DATE,
  notifikovan DATE,

  PRIMARY KEY (id_zaznamu),
  FOREIGN KEY (klient_id) REFERENCES Klienti(id_klienta),
  FOREIGN KEY (dokument_id) REFERENCES Typy_dokumentu(id_dokumentu)
);

-- Filling the roles and type docs (most of time statics) ---------------------------
insert into Role(nazev, popisek) values ("acc", "Ucetni ma pravo na vse.");
insert into Role(nazev, popisek) values ("client", "Klient nema pravo zatim na nic.");

-- TESTING ACC ----------------------------------------------------------------------
insert into Zamestnanci(jmeno, email, heslo, role_id) values ('Fred', 'fred@fim.uhk.cz', 'pass', 1);
insert into Zamestnanci(jmeno, email, heslo, role_id) values ('Jon', 'jon@fim.uhk.cz', 'pass', 1);

-- vystup -> ("danove priznani", 1, "year"); -- 31. 3. yyyy
-- vystup -> ("kontrolni hlaseni", 1, "month"); -- 25. mm. yyyy
insert  into Typy_dokumentu(typ, frekvence) values ('faktury', 'month'); -- 25. mm. yyyy
insert  into Typy_dokumentu(typ, frekvence) values ('mzdy', 'month'); -- 25. mm. yyyy
insert  into Typy_dokumentu(typ, frekvence) values ('pokladni doklady', 'month'); -- 25. mm. yyyy
insert  into Typy_dokumentu(typ, frekvence) values ('bankovni doklady', 'month'); -- 25. mm. yyyy
